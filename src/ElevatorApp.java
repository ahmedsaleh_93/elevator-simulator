import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import elevator.*;

import java.util.Optional;

public class ElevatorApp extends Application {
    private final int BUILDING_HEIGHT = 800;
    private final int ELEVATOR_HEIGHT = 60;
    private int NUMBER_OF_FLOORS = 10;
    private final int FLOOR_HEIGHT = BUILDING_HEIGHT / NUMBER_OF_FLOORS;
    private int NUMBER_OF_ELEVATORS = 3;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        final int ELEVATOR_WIDTH = 30;
        final int ELEVATOR_PANE_WIDTH = ELEVATOR_WIDTH + 30;
        final int ELEVATOR_PANE_HEIGHT = BUILDING_HEIGHT;
        TextInputDialog dialog = new TextInputDialog("enter Number of Elevators");
        dialog.setTitle("Number of Elevators");
        dialog.setHeaderText("Attention");
        dialog.setContentText("Elevators #:");
        Optional<String> result;
        try {
            result = dialog.showAndWait();
            result.ifPresent(elevatorsNum -> NUMBER_OF_ELEVATORS = Integer.parseInt(elevatorsNum));
        } catch (Exception e) {
            System.out.println("wrong entry Number of Elevators is initialized to " + NUMBER_OF_ELEVATORS);
        }
        Elevator.FLOOR_HEIGHT = FLOOR_HEIGHT;
        Elevator[] elevator = new Elevator[NUMBER_OF_ELEVATORS]; // elevators array
        Pane[] elevatorPane = new Pane[NUMBER_OF_ELEVATORS];
        for (int i = 0; i < NUMBER_OF_ELEVATORS; i++) {

            elevator[i] = new Elevator(ELEVATOR_WIDTH, ELEVATOR_HEIGHT, Color.GREENYELLOW, BUILDING_HEIGHT);
            elevator[i].setX((ELEVATOR_PANE_WIDTH - ELEVATOR_WIDTH) / 2.0);
            elevator[i].setY(ELEVATOR_PANE_HEIGHT - ELEVATOR_HEIGHT);
            elevatorPane[i] = new Pane(elevator[i]);
            elevatorPane[i].setMinHeight(ELEVATOR_PANE_HEIGHT);
            elevatorPane[i].setMaxHeight(ELEVATOR_PANE_HEIGHT);
            elevatorPane[i].setMinWidth(ELEVATOR_PANE_WIDTH);
        }
        ElevatorCallPane[] elevatorCallPane = new ElevatorCallPane[NUMBER_OF_FLOORS];
        for (int i = 0; i < NUMBER_OF_FLOORS; i++) {
            elevatorCallPane[i] = new ElevatorCallPane(NUMBER_OF_FLOORS, NUMBER_OF_FLOORS - i);
            ElevatorCallPane tempCallPane = elevatorCallPane[i];
            ToggleGroup toggleGroup;
            toggleGroup = elevatorCallPane[i].getCallPaneToggleGroup();
            elevatorCallPane[i].getCallButton().setOnAction(event -> {
                ToggleButton temp = (ToggleButton) toggleGroup.getSelectedToggle();
                if (temp != null) {
                    int destinationFloorNumber_ = (int) (Integer) temp.getUserData();
                    int destinationFloorPosition_ = (NUMBER_OF_FLOORS - destinationFloorNumber_ + 1) * FLOOR_HEIGHT -
                            ELEVATOR_HEIGHT / 2;

                    int sourceFloorNumber = tempCallPane.getFloorNumber();
                    int sourceFloorPosition = (NUMBER_OF_FLOORS - sourceFloorNumber + 1) * FLOOR_HEIGHT -
                            ELEVATOR_HEIGHT / 2;

                    int optimumElevatorNum = elevatorBrain(elevator, new ElevatorRequest(sourceFloorPosition,
                            destinationFloorPosition_));
                    System.out.println("floor" + destinationFloorNumber_ + "requested from" + sourceFloorNumber);
                    elevator[optimumElevatorNum].addElevatorRequest(new ElevatorRequest(sourceFloorPosition,
                            destinationFloorPosition_));
                    elevator[optimumElevatorNum].setNewElevatorPath();
                }
                toggleGroup.selectToggle(null);

            });
        }
        VBox vBox = new VBox(); // vertical box to hold CallPanes
        vBox.setMinHeight(ELEVATOR_PANE_HEIGHT);
        vBox.getChildren().addAll(elevatorCallPane);
        HBox hBox = new HBox();// Horizontal Box to hold Elevator panes
        hBox.setMaxHeight(ELEVATOR_PANE_HEIGHT);
        hBox.getChildren().addAll(elevatorPane);
        //Grid Pane to layout Call panes and Elevators;
        GridPane gridPane = new GridPane();
        gridPane.setGridLinesVisible(false);
        gridPane.add(vBox, 1, 1);
        gridPane.add(hBox, 2, 1);
        //horizontal Box to hold label for each elevator
        HBox labelHBox = new HBox();
        labelHBox.setAlignment(Pos.CENTER);
        labelHBox.setMinHeight(20);
        labelHBox.setSpacing(10);
        for (int i = 0; i < NUMBER_OF_ELEVATORS; i++) {
            Label tempLabel = new Label("Elev: " + (i + 1));
            // attaching the label to its elevator
            elevator[i].setElevatorLabel(tempLabel);
            tempLabel.setStyle("-fx-background-color: greenyellow");
            tempLabel.setAlignment(Pos.CENTER);
            tempLabel.setMinWidth(ELEVATOR_PANE_WIDTH - 10);
            labelHBox.getChildren().add(tempLabel);
        }
        // adding the Elevators label box to the layout pane
        gridPane.add(labelHBox, 2, 0);

        //Vertical Box to Hold floor Labels
        VBox floorsVBox = new VBox();
        floorsVBox.setAlignment(Pos.CENTER);
        floorsVBox.setMinHeight(ELEVATOR_PANE_HEIGHT);
        for (int i = 0; i < NUMBER_OF_FLOORS; i++) {
            Label tempLabel = new Label("FLOOR: " + (NUMBER_OF_FLOORS-i));

            tempLabel.setStyle("-fx-background-color: skyblue");
            tempLabel.setAlignment(Pos.CENTER);
            tempLabel.setMinWidth(ELEVATOR_PANE_WIDTH);
            tempLabel.setMinHeight(FLOOR_HEIGHT);
            tempLabel.setMaxHeight(FLOOR_HEIGHT);
            floorsVBox.getChildren().add(tempLabel);
        }
        // adding the Floors label box to the layout pane
        gridPane.add(floorsVBox, 0, 1);

        Scene scene = new Scene(gridPane);
        primaryStage.setScene(scene);
        //adjusting the stage dimensions to match its content
        primaryStage.setMinWidth(ElevatorCallPane.CALL_PANE_MIN_WIDTH + ELEVATOR_PANE_WIDTH * NUMBER_OF_ELEVATORS +
                (ELEVATOR_PANE_WIDTH - ELEVATOR_WIDTH) / 2 + ELEVATOR_PANE_WIDTH);
        primaryStage.setMaxWidth(ElevatorCallPane.CALL_PANE_MIN_WIDTH + ELEVATOR_PANE_WIDTH * NUMBER_OF_ELEVATORS +
                (ELEVATOR_PANE_WIDTH - ELEVATOR_WIDTH) / 2);
        primaryStage.setMinHeight(BUILDING_HEIGHT + FLOOR_HEIGHT / 2.0 + labelHBox.getMinHeight());
        primaryStage.setMaxHeight(BUILDING_HEIGHT + FLOOR_HEIGHT / 2.0 + labelHBox.getMinHeight());
        primaryStage.setTitle("Elevator Simulator");
        primaryStage.show();

    }

    private int elevatorBrain(Elevator[] elevators, ElevatorRequest elevatorRequest) {
        double minimumCostTime = 1000000;
        int optimumElevatorNum = 0;
        for (int i = NUMBER_OF_ELEVATORS - 1; i >= 0; i--) {
            double elevatorTimeCost = elevators[i].calculateElevatorTimeCost(elevatorRequest);
            System.out.println("elevator " + i + "cost time =" + elevatorTimeCost);
            if ((long) minimumCostTime >= (long) elevatorTimeCost) {
                minimumCostTime = elevators[i].calculateElevatorTimeCost(elevatorRequest);
                optimumElevatorNum = i;
            }
        }
        System.out.println("optimum Elevator is: Elevator  " + optimumElevatorNum +
                " \n with cost  time =" + minimumCostTime);
        return optimumElevatorNum;
    }
}
