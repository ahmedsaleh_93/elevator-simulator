package  elevator;
public class ElevatorRequest {
    public final static int  FLOOR_HEIGHT = Elevator.FLOOR_HEIGHT;
    private double SourcePosition;
    private double DestinationPosition;
    private double totalRequestsTime;
    public static double calculateTotalRequestsTime(double sourcePosition,double destinationPosition){
        return   1000*Math.abs((sourcePosition - destinationPosition )/FLOOR_HEIGHT);
    }
    public  ElevatorRequest(double requestSource, double requestDestination){
        this.SourcePosition = requestSource;
        this.DestinationPosition = requestDestination;
    }


    public ElevatorRequest() { this(0,0); }

    public double getSourcePosition() {
        return SourcePosition;
    }

    public void setSourcePosition(int sourcePosition) { this.SourcePosition = sourcePosition; }

    public double getDestinationPosition() {
        return DestinationPosition;
    }

    public void setDestinationPosition(int destinationPosition) {
        this.DestinationPosition = destinationPosition;
    }
}
