package elevator;
// this class is designed
import javafx.animation.FillTransition;
import javafx.animation.PathTransition;
import javafx.animation.SequentialTransition;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.VLineTo;
import javafx.util.Duration;

import java.util.ArrayList;

public class Elevator extends Rectangle {

    public static int FLOOR_HEIGHT;
    private final Color OPENED = Color.GREENYELLOW;
    private final Color CLOSED = Color.DARKGREEN;
    private ReadOnlyObjectProperty<Duration> currentAnimationTime;
    private Duration currentAnimationTotalDuration;
    private SimpleIntegerProperty currentPosition;
    private boolean busy;
    private Label elevatorLabel;
    private ArrayList<ElevatorRequest> elevatorRequestQueue;

    public Elevator(int elevatorWidth, int elevatorHeight, Paint color, int buildingHeight) {
        super(elevatorWidth, elevatorHeight, color);
        currentAnimationTotalDuration = new Duration(0);
        busy = false;
        currentPosition = new SimpleIntegerProperty(this, "currentPosition");
        currentPosition.bind(translateYProperty().add(buildingHeight - getHeight() / 2));
        elevatorRequestQueue = new ArrayList<>();
        setStroke(Color.BLACK);
    }

    public void setElevatorLabel(Label elevatorLabel) {
        this.elevatorLabel = elevatorLabel;
    }

    private Duration getCurrentAnimationTotalDuration() {
        return currentAnimationTotalDuration;
    }

    private Duration getCurrentAnimationTime() {
        if (currentAnimationTime != null) {
            return currentAnimationTime.get();
        } else {
            return null;
        }
    }


    private int getCurrentPosition() {
        return currentPosition.get();
    }

    public void addElevatorRequest(ElevatorRequest request) {
        elevatorRequestQueue.add(request);
    }

    public void setNewElevatorPath() {
        //get the next request for the elevator and then removing it once handled
        if ((elevatorRequestQueue.size() > 0) && (!busy)) {
            busy = true;
            PathTransition currentPositionToSourcePathTransition = new PathTransition();
            currentPositionToSourcePathTransition.setCycleCount(1);
            PathTransition sourceToDestinationPathTransition = new PathTransition();
            sourceToDestinationPathTransition.setCycleCount(1);
            ElevatorRequest elevatorRequest = elevatorRequestQueue.get(0);
            //calculating the duration for the current position to source transition
            double currentPositionToSourceDuration = 1000 * Math.abs(
                    (currentPosition.doubleValue() - elevatorRequest.getSourcePosition()) / FLOOR_HEIGHT);
            //calculating the duration for the  source to destination transition

            double sourceToDestinationDuration = 1000 * Math.abs(
                    (elevatorRequest.getSourcePosition() - elevatorRequest.getDestinationPosition()) / FLOOR_HEIGHT);
            // setting the path and duration for  the current position to source transition
            currentPositionToSourcePathTransition.setDuration(Duration.millis(currentPositionToSourceDuration));
            currentPositionToSourcePathTransition.setPath(new Path(new MoveTo(getX() + getWidth() / 2,
                    currentPosition.getValue()), new VLineTo(elevatorRequest.getSourcePosition())));

            // setting the path and duration for  the source to destination transition
            sourceToDestinationPathTransition.setDuration(Duration.millis(sourceToDestinationDuration));
            sourceToDestinationPathTransition.setPath(new Path(new MoveTo(getX() + getWidth() / 2, elevatorRequest.getSourcePosition()),
                    new VLineTo(elevatorRequest.getDestinationPosition())));
            // setting the sequential animation Path for Elevator Request
            SequentialTransition elevatorSequentialTransition;
            //elevatorSequentialTransition.getChildren().clear();
            if (currentPositionToSourceDuration < 10) {
                elevatorSequentialTransition = new SequentialTransition(this,
                        new FillTransition(Duration.millis(1000), OPENED, CLOSED),
                        sourceToDestinationPathTransition,
                        new FillTransition(Duration.millis(1000), CLOSED, OPENED),
                        new FillTransition(Duration.millis(2000), OPENED, OPENED));
                System.out.println(elevatorSequentialTransition.getTotalDuration().toSeconds());
            } else {
                elevatorSequentialTransition = new SequentialTransition(this,
                        new FillTransition(Duration.millis(1000), OPENED, CLOSED),
                        currentPositionToSourcePathTransition,
                        new FillTransition(Duration.millis(1000), CLOSED, OPENED),
                        new FillTransition(Duration.millis(2000), OPENED, OPENED),
                        new FillTransition(Duration.millis(1000), OPENED, CLOSED),
                        sourceToDestinationPathTransition,
                        new FillTransition(Duration.millis(1000), CLOSED, OPENED),
                        new FillTransition(Duration.millis(2000), OPENED, OPENED));
            }
            currentAnimationTime = elevatorSequentialTransition.currentTimeProperty();
            currentAnimationTotalDuration = elevatorSequentialTransition.getTotalDuration();
            elevatorSequentialTransition.setOnFinished(event -> {
                elevatorLabel.setStyle("-fx-background-color: greenyellow");
                currentAnimationTotalDuration = new Duration(0);
                elevatorRequestQueue.remove(0);
                busy = false;
                if (elevatorRequestQueue.size() > 0) {
                    setNewElevatorPath();
                }
                System.out.println("getTranslateY() :" + currentPosition.get());
                //currentPosition.setValue(getTranslateY());
            });
            elevatorSequentialTransition.play();
            elevatorLabel.setStyle("-fx-background-color: red");
            System.out.println(elevatorSequentialTransition.getCurrentTime().toSeconds());

        }
    }

    public double calculateElevatorTimeCost(ElevatorRequest elevatorRequest) {
        double timeCost = 0;
        if (elevatorRequestQueue.size() > 0) {
            try {
                timeCost += getCurrentAnimationTotalDuration().toMillis();
            } catch (Exception e) {
                System.out.println("timeCost += getCurrentAnimationTotalDuration().toMillis();");
            }
            if (getCurrentAnimationTime() != null) {
                timeCost -= getCurrentAnimationTime().toMillis();
            }else{
                System.out.println("timeCost -= getCurrentAnimationTime().toMillis();");
            }

            int i;
            for (i = 1; i < elevatorRequestQueue.size(); i++) {
                if (Math.abs(elevatorRequestQueue.get(i - 1).getDestinationPosition() -
                        elevatorRequestQueue.get(i).getSourcePosition())
                        > 1) {
                    timeCost += 8000;
                    timeCost += ElevatorRequest.calculateTotalRequestsTime(
                            elevatorRequestQueue.get(i - 1).getDestinationPosition(),
                            elevatorRequestQueue.get(i).getSourcePosition());
                } else {
                    timeCost += 4000;
                }

                timeCost += ElevatorRequest.calculateTotalRequestsTime(
                        elevatorRequestQueue.get(i).getSourcePosition(),
                        elevatorRequestQueue.get(i).getDestinationPosition());

            }
            i--;
            timeCost += ElevatorRequest.calculateTotalRequestsTime(
                    elevatorRequestQueue.get(i).getDestinationPosition(),
                    elevatorRequest.getSourcePosition());

        } else {
            timeCost += ElevatorRequest.calculateTotalRequestsTime(
                    getCurrentPosition(),
                    elevatorRequest.getSourcePosition());

        }
        return timeCost;
    }
}

