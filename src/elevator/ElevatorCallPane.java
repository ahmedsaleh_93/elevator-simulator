package elevator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;

public class ElevatorCallPane extends FlowPane {
    public  static int CALL_PANE_MIN_WIDTH;

    private ToggleGroup callPaneToggleGroup = new ToggleGroup();

    public Button getCallButton() {
        return callButton;
    }

    public ToggleGroup getCallPaneToggleGroup() {
        return callPaneToggleGroup;
    }
    private Button callButton ;

    private int floorNumber;

    public int getFloorNumber() {
        return floorNumber;
    }

    public ElevatorCallPane(int numOfFloors, int floorNumber) {
        super();
        final int CALL_PANE_HEIGHT = Elevator.FLOOR_HEIGHT;
        final int CALL_PANE_GAP = 2;
        final Insets PADDING_INSETS = new Insets(0,6,0,6);
        final int BUTTON_SIZE = 30;
        ToggleButton[] callToggleButton;
        CALL_PANE_MIN_WIDTH = (numOfFloors)*BUTTON_SIZE/2+(numOfFloors+1)*CALL_PANE_GAP;
        setAlignment(Pos.BOTTOM_CENTER);
        callToggleButton = new ToggleButton[numOfFloors];
        this.floorNumber = floorNumber;
        setMinWidth(CALL_PANE_MIN_WIDTH);
        setMaxWidth(CALL_PANE_MIN_WIDTH);
        setMinHeight(CALL_PANE_HEIGHT);
        setHgap(CALL_PANE_GAP);
        setVgap(CALL_PANE_GAP);
        setPadding(PADDING_INSETS);
        for (int i=0;i<numOfFloors;i++){
            callToggleButton[i] = new ToggleButton(""+(i+1));
            callToggleButton[i].setFont(new Font(12));
            callToggleButton[i].setMinSize(BUTTON_SIZE,BUTTON_SIZE);
            callToggleButton[i].setMaxSize(BUTTON_SIZE,BUTTON_SIZE);

            callToggleButton[i].setToggleGroup(callPaneToggleGroup);
            callToggleButton[i].setUserData(i+1);
            //callToggleButton[i].setStyle("-fx-background-color: #d3bfb5");
            callToggleButton[i].setOnAction(event -> {
            });
            callToggleButton[i].selectedProperty().addListener((ov, oldValue, newValue) ->{
            });
            if(i==(floorNumber -1)) {
                callToggleButton[i].setDisable(true);
            } else
                {
                    getChildren().add( callToggleButton[i]);
                }
        }
        callButton = new Button("C");
        System.out.println(callButton.getStyle());
        callButton.setStyle("-fx-border-color: red ");
        callButton.setMinSize(BUTTON_SIZE,BUTTON_SIZE);
        getChildren().add(callButton);
        callButton.setOnAction(event -> {

           ToggleButton temp = (ToggleButton) callPaneToggleGroup.getSelectedToggle();
            if(temp!=null){
                System.out.println("floor"+ temp.getUserData()+"requested from" +floorNumber );
                //temp.setTextFill(Color.BLACK);
            }
            callPaneToggleGroup.selectToggle(null);


            //temp.setSelected(false);

        });

    }


}
